# Collection of roles for ONAP automatic installation via OOM

This project aims to automatically install ONAP. Its config source
is shared config files among all OPNFV installers:

## Inputs

If you use directly the roles, see [roles documentation directory](docs/roles)
files to see the default values that can be set.

If you use playbooks, overriden variables can be set in a file. Per default,
this file is named `idf.yml` and is put in `vars` folder at root part of this
collection.

see also [playbook environment variables](
docs/playbooks/environment_variables.md) that can be set instead of overriding
them.

## Output

- artifacts:
  - vars/cluster.yml
