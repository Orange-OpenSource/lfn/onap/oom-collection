---
- name: check if a environment.yaml exists
  ansible.builtin.stat:
    path: "{{ onap_generic_override_path }}/environment.yaml"
  register: environment_stat

- name: set environment.yaml override
  ansible.builtin.set_fact:
    onap_environment_override:
      "--values {{ onap_generic_override_path }}/environment.yaml"
  when: environment_stat.stat.exists

- name: do not set environment.yaml override
  ansible.builtin.set_fact:
    onap_environment_override: ""
  when: not environment_stat.stat.exists

- name: check if a onap-components.yml exists
  ansible.builtin.stat:
    path: "{{ onap_override_components }}"
  register: component_stat

- name: set onap-components.yml override
  ansible.builtin.set_fact:
    onap_component_override: "--values {{ onap_override_components }}"
  when: component_stat.stat.exists

- name: do not set onap-components.yml override
  ansible.builtin.set_fact:
    onap_component_override: ""
  when: not component_stat.stat.exists

- name: check if a component-gating-overrides.yml exists
  ansible.builtin.stat:
    path: "{{ onap_override_gating_component }}"
  register: gating_stat

- name: set component-gating-overrides.yml override
  ansible.builtin.set_fact:
    onap_gating_override: "--values {{ onap_override_gating_component }}"
  when: gating_stat.stat.exists and
    (project == "so" or project == "clamp")

- name: do not set component-gating-overrides.yml override
  ansible.builtin.set_fact:
    onap_gating_override: ""
  when: not gating_stat.stat.exists or
    (project != "so" and project != "clamp")

- name: create ONAP namespace
  run_once: true
  kubernetes.core.k8s:
    state: present
    definition:
      apiVersion: v1
      kind: Namespace
      metadata:
        name: "{{ onap_namespace }}"
        labels:
          istio-injection: "{{ onap_istio_enabled | bool |
            ternary ('enabled', 'disabled') }}"
          name: "{{ onap_namespace }}"

- name: generate privileged configuration for service accounts
  ansible.builtin.include_tasks: pod_security_policy.yml
  when: kubernetes_privileged_pod_security_policy is defined

- name: generate command line for launch
  ansible.builtin.set_fact:
    onap_helm_launch: >
      helm deploy {{ onap_release_name }} local/onap
        --kubeconfig {{ onap_kubeconfig_path }}
        --namespace {{ onap_namespace }}
        --version {{ onap_release_version }}
        --values {{ onap_all_file }}
        {{ onap_environment_override }}
        --values {{ onap_override_file }}
        {{ onap_component_override }}
        {{ onap_gating_override }}
        --timeout {{ onap_timeout }}

- name: debug command sent
  debug:
    msg: "{{ onap_helm_launch }}"

- name: launch installation
  ansible.builtin.command: "{{ onap_helm_launch }}"
  register: launch
  changed_when: true
  async: 4800
  poll: 0

- name: wait for helm deploy to finish
  ansible.builtin.async_status:
    jid: "{{ launch.ansible_job_id }}"
  register: job_result
  until: job_result.finished
  retries: 480
  delay: 10

- name: see output
  ansible.builtin.debug:
    msg: "{{ job_result.stdout }}"

- name: grab a beer
  ansible.builtin.debug:
    msg: "          .:.\n        _oOoOo\n       \
      [_|||||\n         |||||\n         ~~~~~"
