FROM registry.gitlab.com/orange-opensource/lfn/ci_cd/docker_ansible_openstacksdk:2.10-alpine
LABEL maintainer="Sylvain Desbureaux <sylvain.desbureaux@orange.com>"

ARG VCS_REF
ARG BUILD_DATE

ENV APP /opt/oom-collection/
ENV COLLECTION_PATH ~/.ansible/collections

WORKDIR $APP

COPY . $APP/

RUN mkdir -p "$COLLECTION_PATH" && \
    ansible-galaxy collection install "git+file://$PWD/.git" \
      -p "$COLLECTION_PATH" && \
    rm -rf "$APP/.git"
