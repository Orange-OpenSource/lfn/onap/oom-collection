# OOM collection

The goal of this collection consists in deploying ONAP.
This collection is a community collection provided by the OOM
(ONAP Operation Manager) project.

It consists in a list of playbook consuming several roles.

## Playbooks
<!-- markdownlint-disable line-length -->
| Playbook                                                      | Description                                                         |
| ------------------------------------------------------------- | ------------------------------------------------------------------- |
| [clean](./playbooks/clean.md)                                 | Clean the ONAP solution                                             |
| [prepare](./playbooks/prepare.md)                             | Prepare the ONAP installation (prerequisites)                       |
| [environment variables](./playbooks/environment_variables.md) | Manage the ONAP env variables                                       |
| [configure](./playbooks/configure.md)                         | Configure the ONAP solution                                         |
| [deploy](./playbooks/deploy.md)                               | Deploy the ONAP solution                                            |
| [postconfigure](./playbooks/postconfigure.md)                 | Postconfigure operation to be executed on a fresh ONAP installation |
| [postinstallation](./playbooks/postinstall.md)                | Postinstall operations                                              |
| [wait](./playbooks/wait.md)                                   | Check and wait until an ONAP installation is completed              |
<!-- markdownlint-enable line-length -->

## Roles

<!-- markdownlint-disable line-length -->
| Role                                                      | Description                                     |
| --------------------------------------------------------- | ----------------------------------------------- |
| [clean](./roles/clean.md)                                 | Role associated with the clean playbook         |
| [prepare](./roles/prepare.md)                             | Role associated with the prepare playbook       |
| [configure](./roles/configure.md)                         | Role associated with the configure playbook     |
| [launch](./roles/launch.md)                               | Role associated with the deploy playbook        |
| [postconfigure](./roles/postconfigure.md)                 | Role associated with the postconfigure playbook |
| [wait](./roles/wait.md)                                   | Role associated with the wait playbook          |
| [external requirements](./roles/external_requirements.md) | Utility role to manage external requirements    |
| [generate artifacts](./roles/generate_artifacts.md)       | Utility role to generate artifacts              |
| [include_vars](./roles/include_vars.md)                   | Utility role to includ vars                     |
<!-- markdownlint-enable line-length -->

## Versions

| ONAP version   | Collection sha1                          |
| -------------- | ---------------------------------------- |
| Istanbul       | 26cfc2ee4fab4f9269c6e66f02f6147aa4886748 |
