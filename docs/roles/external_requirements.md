# External requirements role

## Purpose

This role will install all needed requirements for ONAP installation.

## Parameters

<!-- markdownlint-disable line-length -->
| Variable                                                | Purpose                                                       | Default value                                                          |
|---------------------------------------------------------|---------------------------------------------------------------|------------------------------------------------------------------------|
| `python_pip_repository`                                 | repository for "pip"                                          | <https://pypi.org/simple/>                                             |
| `onap_path`                                             | path to OOM source code                                       | `{{ ansible_user_dir }}/onap/oom`                                      |
| `chartmuseum_to_install_on_target`                      | do we install Chartmuseum on target                           | `true`                                                                 |
| `docker_nerdctl_repository`                             | url for nerdctl release                                       | <https://github.com/containerd/nerdctl/releases/download>              |
| `docker_nerdctl_release`                                | version of nerdctl to install                                 | `0.8.0`                                                                |
| `docker_nerdctl_sha256`                                 | sha256 of nerdctl release artifact                            | The one for `0.8.0`                                                    |
| `docker_nerdctl_install`                                | do we install nerdctl                                         | `False`                                                                |
| `docker_nerdctl_path`                                   | path to nerdctl binary                                        | `/usr/local/bin/nerdctl`                                               |
| `helm_server_name`                                      | used to define the url of helm repository                     | `127.0.0.1`                                                            |
| `helm_server_port`                                      | used to define the port of helm repository                    | `8879`                                                                 |
| `kubernetes_charts_helm_chartmuseum_logs_folder`        | where to log chartmuseum actions                              | `{{ ansible_user_dir }}/.local/chartmuseum`                            |
| `kubernetes_charts_helm_chartmuseum_logs_file`          | file for log                                                  | `{{ kubernetes_charts_helm_chartmuseum_logs_folder }}/chartmuseum.log` |
| `kubernetes_charts_helm_chartmuseum_path`               | path to chartmuseum binary                                    | `/usr/local/bin/chartmuseum`                                           |
| `kubernetes_charts_helm_chartmuseum_sha256`             | sha256 of chartmuseum release artifact                        | The one for `v0.13.1`                                                  |
| `kubernetes_charts_helm_chartmuseum_storage_dir`        | where chartmuseum will store pushed charts                    | `{{ ansible_user_dir }}/.chartstorage`                                 |
| `kubernetes_charts_helm_chartmuseum_timeout_wait_start` | wait time before stating that chartmuseum didn't started well | `10` (seconds)                                                         |
| `kubernetes_charts_helm_chartmuseum_version`            | version of chartmuseum to install                             | `v0.13.1`                                                              |
| `kubernetes_charts_helm_repository`                     | url for chartmuseum release                                   | <https://get.helm.sh>                                                  |
| `kubernetes_charts_helm_plugins_push_release`           | version of helm push plugin to install                        | `0.9.0`                                                                |
| `kubernetes_charts_helm_plugins_push_repository`        | url for helm push plugin release                              | <https://github.com/chartmuseum/helm-push/releases/download>           |
| `kubernetes_charts_helm_plugins_push_sha256`            | sha256 of helm push plugin release artifact                   | The one for `0.9.0`                                                    |
<!-- markdownlint-enable line-length -->

The url of helm repository is built under this form
`http://{{ helm_server_name }}:{{ helm_server_port }}/`

### ⚠️  version and sha256

when setting a new version, you must also give the new sha256 fingerprint.
If not done, the download will fail as sha256 number will be different

### nerdctl

Configuration role needs to have access to a "docker-like" cli in order to
generate encrypted passwords.
As we may not have docker, we can install nerdctl which will act as docker when
only containerd is installed.
Please bear in mind that configuration on configure must be set accordingly.
For example, when using rke2, we need to configure the following on
**configure** role ((`docker_nerdctl_path` must be "translated" or given to
configure role)):

```yaml
docker_executable:
  '{{ docker_nerdctl_path }} -a unix:///var/run/k3s/containerd/containerd.sock'
docker_nerdctl_install: true
```

## Examples

```yaml
- ansible.builtin.import_role:
    name: orange.oom.external_requirements

- ansible.builtin.import_role:
    name: orange.oom.external_requirements
  vars:
    python_pip_repository: https://my-mirror/
```
