
# Postconfigure role

## Purpose

Create a complex and multicloud vim configuration on an ONAP.

This part works (by default) using nodeports and msb is mandatory if you want to
configure a vim in multicloud.

## Parameters

<!-- markdownlint-disable line-length -->
| Variable                 | Purpose                         | Default value                                         |
|--------------------------|---------------------------------|-------------------------------------------------------|
| `aai_server`             | AAI URL                         | `aai.api.sparky.simpledemo.onap.org`                  |
| `aai_port`               | AAI port                        | `30233`                                               |
| `aai_user`               | AAI user                        | `AAI`                                                 |
| `aai_password`           | AAI password                    | `AAI`                                                 |
| `msb_server`             | MSB URL                         | `msb.api.simpledemo.onap.org`                         |
| `msb_port`               | MSB port                        | `30283`                                               |
| `datacenter_id`          | ID of the datacenter            | `cevt`                                                |
| `datacenter_code`        | Code of the datacenter          | `LAI-CEVT`                                            |
| `datacenter_url`         | URL of the datacenter           | `http://opnfv.pages.forge.orange-labs.fr/TerraHouat/` |
| `datacenter_name`        | Name of the datacenter          | `CEVT`                                                |
| `datacenter_type`        | Type of the datacenter          | `datacenter`                                          |
| `datacenter_street1`     | Street of the datacenter        | `Avenue Pierre Marzin`                                |
| `datacenter_street2`     | Second Street of the datacenter | Not set                                               |
| `datacenter_city`        | City of the datacenter          | `Lannion`                                             |
| `datacenter_state`       | State of the datacenter         | `Brittany`                                            |
| `datacenter_postal_code` | Postal code of the datacenter   | `22300`                                               |
| `datacenter_country`     | Country of the datacenter       | `Lannion`                                             |
| `datacenter_region`      | Region of the datacenter        | `Europe`                                              |
| `datacenter_latitude`    | Latitude of the datacenter      | `48.7606787`                                          |
| `datacenter_longitude`   | Longitude of the datacenter     | `-3.45038047`                                         |
| `datacenter_elevation`   | Elevation of the datacenter     | `92`                                                  |
| `datacenter_lata`        | LATA of the datacenter          | `02`                                                  |
| `datacenter_owner`       | Owner of the datacenter         | `Orange`                                              |
| `datacenter_contact`     | Contact of the datacenter       | `N/A`                                                 |
| `datacenter_lab`         | Lab of the datacenter           | `cevt`                                                |
<!-- markdownlint-enable line-length -->

## Examples

```yaml
- ansible.builtin.import_role:
    name: orange.oom.postconfigure

- ansible.builtin.import_role:
    name: orange.oom.postconfigure
  vars:
    datacenter_owner: OEG
```
