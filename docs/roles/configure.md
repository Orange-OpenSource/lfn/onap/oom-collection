# Configure role

## Purpose

Configure ONAP before installing it.

## Parameters
<!-- markdownlint-disable line-length -->
| Variable                                             | Purpose                                           | Default value                                    |
|------------------------------------------------------|---------------------------------------------------|--------------------------------------------------|
| `base_dir`                                           | root directory on Ansible controller              | `{{ inventory_dir }}/..`                         |
| `oom_node_group`                                     | group name for kubernetes workers                 | `kube-node`                                      |
| `datacenter_owner`                                   | "owner" of the datacenter                         | `Orange`                                         |
| `gather_nodes_fact`                                  | do we have gathered facts from kubernetes workers | `True`                                           |
| `gerrit_review`                                      | review number of gerrit merge request             | Not set                                          |
| `gerrit_patchset`                                    | patchset number of gerrit merge request           | Not set                                          |
| `gerrit_project`                                     | gerrit merge request project                      | `oom`                                            |
| `kubernetes_docker_proxy`                            | name for docker main repository                   | `docker.io`                                      |
| `kubernetes_minio_internal_installation`             | do we have a minio internal installation          | `False`                                          |
| `kubernetes_minio_internal_installation_use_ingress` | does minio internal installation use ingress      | `True`                                           |
| `onap_additional_components`                         | list of deployments we want to add                | `[]`                                             |
| `onap_deployment_type`                               | type of deployment (in term of component) we want | `full`                                           |
| `onap_etc_path`                                      | path where to store onap configuration files      | `{{ ansible_user_dir }}/onap/{{ onap_version }}` |
| `onap_flavor`                                        | flavor of deployment (in term of size) we want    | `unlimited`                                      |
| `onap_given_component_override_file`                 | name of the optional component override file      | `components-overrides.yml`                       |
| `onap_global_storage`                                | do we want to use a storage class                 | `True`                                           |
| `onap_global_storage_class`                          | name of main storage class                        | `-`                                              |
| `onap_global_storage_fast_class`                     | name of fast storage class                        | Not set                                          |
| `onap_global_storage_rwx_class`                      | name of rwx storage class release                 | Not set                                          |
| `onap_path`                                          | path to OOM source code                           | `{{ ansible_user_dir }}/onap/oom`                |
| `onap_repository_principal`                          | url for onap docker repository                    | `nexus3.onap.org:10001`                          |
| `onap_repository_docker_hub`                         | url for docker main repository                    | `docker.io`                                      |
| `onap_repository_elastic`                            | url for docker elastic repository                 | `docker.elastic.co`                              |
| `onap_repository_k8s_google`                         | url for docker k8s repository                     | `k8s.gcr.io`                                     |
| `onap_use_ingress`                                   | do we want Ingress                                | `True`                                           |
| `onap_version`                                       | version of OOM to use                             | `master`                                         |
| `openstack_user_name`                                | user name to use on OpenStack                     | `default`                                        |
| `openstack_tenant_name`                              | tenant name to use on OpenStack                   | `default`                                        |
| `openstack_service_tenant_name`                      | service tenant name to use on OpenStack           | `service`                                        |
| `docker_executable`                                  | name of docker (like) binary                      | `docker`                                         |
| `docker_run_as_root`                                 | does docker (like) must be launched as root       | `False`                                          |
<!-- markdownlint-enable line-length -->

### Gathering nodes fact

In order to configure internal `minio` (possibly needed for tests), we need to
know the first node IP if `minio` is not using Ingress.

We can either use "ansible facts" or rely on a "well known" already set variable
(`ip`).

If `minio` is internal and is not using Ingress, we then need to:

* have a group where at least one Kubernetes worker node is present
  (`kube-node` per default)
* know it's IP address (either via an already done fact gathering or via the
  well known variable -- `ip` -- )

### Datacenter Owner

if you install `nbi` component from ONAP, it'll need to know the Datacenter
owner.

Please bear in mind that this value must be the same given in `postconfigure`
role.

### Gerrit

This installation can be used in order to validate gerrit merge requests from
other projects (clamp and so for now).
If so, you'll need to give:

* the `gerrit_project` that needs to be validated
* the `gerrit_patchset` and `gerrit_review` so proper configuration can be done

### Storage

ONAP can be installed with a dynamic or static PVC.
For dynamic PVC, we need to set the storage class to use (OOM doesn't support
using default storage class).
We can also define two specific storage class (if not set, main storage class
will be used):

* one for RWX PVC: ReadWriteMany PVC may need a specific storage class so we can
  set it here
* one for Datastore: these components may need a storage with high IOPS

### Docker

in order to generate "encrypted" passwords of Openstack user for SO, we need to
use a docker(like) command.
By default, we run docker as non root.
Both things can be changed.****

### Generating a deployment configuration

there are 2 ways of generating the configuration:

* give a specific configuration via a file in the Ansible controller (
  `{{ base_dir }}/vars/{{ onap_given_component_override_file }}`)
* set a deployment types and optionnally and additional component list

#### Deployment types

Here are the four known deployment types (to be used in `onap_deployment_type`):

* core: aaf, aai, dmaap, robot, sdc, sdnc, so
* small: core + appc, cli, esr, log, msb, multicloud, nbi, portal, vid
* medium: small + clamp, contrib, dcaegen2, oof, policy, pomba
* full: all onap components

#### Additional components

Here's the list of components available (to be added via a list in
`onap_additional_components`):

* all medium components
* modeling
* vnfsdk
* vfc
* uui
* sniro_emulator

## Examples

```yaml
- ansible.builtin.import_role:
    name: orange.oom.configure

- ansible.builtin.import_role:
    name: orange.oom.configure
  vars:
    onap_release_name: oronap
```
