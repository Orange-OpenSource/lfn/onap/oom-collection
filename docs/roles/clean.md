# Clean role

## Purpose

Clean a previous ONAP installation

## Parameters

<!-- markdownlint-disable line-length -->
| Variable            | Purpose                      | Default value                     |
|---------------------|------------------------------|-----------------------------------|
| `onap_namespace`    | name where ONAP is installed | `onap`                            |
| `onap_path`         | where OOM source code is put | `{{ ansible_user_dir }}/onap/oom` |
| `onap_release_name` | name of onap release         | `onap`                            |
<!-- markdownlint-enable line-length -->

## Examples

```yaml
- ansible.builtin.import_role:
    name: orange.oom.clean

- ansible.builtin.import_role:
    name: orange.oom.clean
  vars:
    onap_release_name: oronap
```
