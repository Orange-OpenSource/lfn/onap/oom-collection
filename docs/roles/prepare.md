# Prepare role

## Purpose

Prepare system for ONAP installation:

* retrieve OOM code
* push charts into repository
* get OpenStack information

## Parameters

<!-- markdownlint-disable line-length -->
| Variable                                    | Purpose                                    | Default value                   |
|---------------------------------------------|--------------------------------------------|---------------------------------|
| `docker_executable`                         | name of docker (like) binary               | `docker`                        |
| `kubernetes_restricted_pod_security_policy` | name for the global restricted policy      | `global-restricted-psp`         |
| `gerrit_review`                             | review number of gerrit merge request      | Not set                         |
| `gerrit_patchset`                           | patchset number of gerrit merge request    | Not set                         |
| `gerrit_project`                            | gerrit merge request project               | `oom`                           |
| `helm_server_name`                          | used to define the url of helm repository  | `127.0.0.1`                     |
| `helm_server_port`                          | used to define the port of helm repository | `8879`                          |
| `onap_git_url`                              | URL of OOM git repository                  | <https://gerrit.onap.org/r/oom> |
| `onap_parent_path`                          | parent path of OOM source code folder      | `{{ ansible_user_dir }}/onap`   |
| `onap_path`                                 | where OOM source code is put               | `{{ onap_parent_path }}/oom`    |
| `onap_retrieve_from_controller`             | Do we retrieve OOM on controller?          | `False`                         |
| `onap_version`                              | version of OOM to use                      | `master`                        |
<!-- markdownlint-enable line-length -->

The url of helm repository is built under this form
`http://{{ helm_server_name }}:{{ helm_server_port }}/`

### Retrieving OOM code

We need to retrieve OOM code for building the charts and for helm deploy plugin.
If your kubernetes doesn't have direct access, then you need to retrieve it from
your Ansible controller (a.k.a. the machine where you've launched the role).

### Gerrit

This installation can be used in order to validate gerrit merge requests from
OOM or other projects (clamp and so for now).
If so, you'll need to give:

* the `gerrit_project` that needs to be validated
* the `gerrit_patchset` and `gerrit_review` so proper configuration can be done

## Examples

```yaml
- ansible.builtin.import_role:
    name: orange.oom.prepare

- ansible.builtin.import_role:
    name: orange.oom.prepare
  vars:
    onap_version: honolulu
```
