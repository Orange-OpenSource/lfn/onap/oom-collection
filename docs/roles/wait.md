# Wait role

## Purpose

Wait for an ONAP installation to be installed

## Parameters

<!-- markdownlint-disable line-length -->
| Variable            | Purpose                          | Default value                                    |
|---------------------|----------------------------------|--------------------------------------------------|
| `onap_etc_path`     | path to onap configuration files | `{{ ansible_user_dir }}/onap/{{ onap_version }}` |
| `onap_namespace`    | name where ONAP is installed     | `onap`                                           |
| `onap_release_name` | name of onap release             | `onap`                                           |
| `onap_version`      | version of onap                  | `master`                                         |
<!-- markdownlint-enable line-length -->

## Examples

```yaml
- ansible.builtin.import_role:
    name: orange.oom.wait

- ansible.builtin.import_role:
    name: orange.oom.wait
  vars:
    onap_release_name: oronap
```
