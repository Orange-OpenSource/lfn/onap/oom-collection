# Generate artifacts role

## Purpose

This role has two main purposes:

* generating /etc/hosts part for accessing onap components
* generating a `cluster.yml` file with needed information for tests for example

## Parameters

<!-- markdownlint-disable line-length -->
| Variable            | Purpose                                           | Default value            |
|---------------------|---------------------------------------------------|--------------------------|
| `base_dir`          | root directory on Ansible controller              | `{{ inventory_dir }}/..` |
| `oom_node_group`    | group name for kubernetes workers                 | `kube-node`              |
| `gather_nodes_fact` | do we have gathered facts from kubernetes workers | `True`                   |
| `onap_namespace`    | name where ONAP is installed                      | `onap`                   |
<!-- markdownlint-enable line-length -->

## Gathering nodes fact

In order to configure internal address for ONAP components, we need to
know the first node IP (it may not be necessary when we'll use Ingress but it's
not done yet).

We can either use "ansible facts" or rely on a "well known" already set variable
(`ip`).

## Examples

```yaml
- ansible.builtin.import_role:
    name: orange.oom.generate_artifacts

- ansible.builtin.import_role:
    name: orange.oom.generate_artifacts
  vars:
    onap_namespace: oronap
```
