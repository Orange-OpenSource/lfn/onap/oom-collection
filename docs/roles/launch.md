# Launch role

## Purpose

Launch an installation of ONAP via OOM and configured files

## Parameters

<!-- markdownlint-disable line-length -->
| Variable                                    | Purpose                                      | Default value                                           |
|---------------------------------------------|----------------------------------------------|---------------------------------------------------------|
| `kubernetes_privileged_pod_security_policy` | name for the global privileged policy        | Not Set                                                 |
| `onap_etc_path`                             | path where to store onap configuration files | `{{ ansible_user_dir }}/onap/{{ onap_version }}`        |
| `onap_istio_enabled`                        | do we want to have istio sidecar injection   | `True`                                                  |
| `onap_namespace`                            | name where ONAP is installed                 | `onap`                                                  |
| `onap_path`                                 | where OOM source code is put                 | `{{ ansible_user_dir }}/onap/oom`                       |
| `onap_release_name`                         | name of onap release                         | `onap`                                                  |
| `onap_release_version`                      | version of onap release                      | "official" version from OOM according to `onap_version` |
| `onap_version`                              | version of onap                              | `master`                                                |
| `onap_timeout`                              | timeout for helm component installation      | `900s`                                                  |
<!-- markdownlint-enable line-length -->

## Examples

```yaml
- ansible.builtin.import_role:
    name: orange.oom.launch

- ansible.builtin.import_role:
    name: orange.oom.launch
  vars:
    onap_release_name: oronap
```
