# Include vars role

## Purpose

Check if a property file exists on Ansible Controller and loads it if present

## Parameters

| Variable        | Purpose                      | Default value            |
|-----------------|------------------------------|--------------------------|
| `property_file` | property file to load        | `idf.yml`                |
| `base_dir`      | folder where is file to load | `{{ inventory_dir }}/..` |

## Examples

```yaml
- ansible.builtin.import_role:
    name: orange.oom.include_vars

- ansible.builtin.import_role:
    name: orange.oom.include_vars
  vars:
    property_file: openstack.yml
```
