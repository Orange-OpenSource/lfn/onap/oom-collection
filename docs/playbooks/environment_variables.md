# Environment variables

## Purpose

It maybe sometimes simpler to give environment variables to change behavior of
the installer without giving a specific file.
If a file for override is also given, precedence will take place in favor of
variables set in the file rather than environment variable.

## Variables

<!-- markdownlint-disable line-length -->
| Variable                | Overriden variable           | Purpose                                           | Default Value                |
|-------------------------|------------------------------|---------------------------------------------------|------------------------------|
| `RUN_ROOT`              | `base_dir`                   | folder where is file to load                      | `{{ inventory_dir }}/..`     |
| `GATHER_NODE_FACTS`     | `gather_nodes_fact`          | do we have gathered facts from kubernetes workers | `True`                       |
| `GERRIT_REVIEW`         | `gerrit_review`              | review number of gerrit merge request             | Not set                      |
| `GERRIT_PATCHSET`       | `gerrit_patchset`            | patchset number of gerrit merge request           | Not set                      |
| `ADDITIONAL_COMPONENTS` | `onap_additional_components` | list of deployments we want to add                | Not set                      |
| `DEPLOYMENT_TYPE`       | `onap_deployment_type`       | type of deployment (in term of component) we want | `full`                       |
| `ONAP_FLAVOR`           | `onap_flavor`                | flavor of deployment (in term of size) we want    | `unlimited`                  |
| `ONAP_NAMESPACE`        | `onap_namespace`             | folder where is file to load                      | `onap`                       |
| `ONAP_CHART_NAME`       | `onap_release_name`          | folder where is file to load                      | `{{ onap_namespace }}`       |
| `ONAP_REPOSITORY`       | `onap_repository_principal`  | url for onap docker repository                    | `nexus3.onap.org:10001`      |
| `DOCKER_HUB_PROXY`      | `onap_repository_docker_hub` | url for docker main repository                    | `docker.io`                  |
| `ELASTIC_PROXY`         | `onap_repository_elastic`    | url for docker elastic repository                 | `docker.elastic.co`          |
| `K8S_GCR_PROXY`         | `onap_repository_k8s_google` | url for docker k8s repository                     | `k8s.gcr.io`                 |
| `INGRESS`               | `onap_use_ingress`           | do we want Ingress                                | `True`                       |
| `OOM_BRANCH`            | `onap_version`               | version of OOM to use                             | `master`                     |
| `TENANT_NAME`           | `openstack_tenant_name`      | tenant name to use on OpenStack                   | `{{ os_infra.tenant.name }}` |
| `USER_NAME`             | `openstack_user_name`        | user name to use on OpenStack                     | `{{ os_infra.user.name }}`   |
| `PROJECT`               | `gerrit_project`             | gerrit merge request project                      |  `oom`                       |

<!-- markdownlint-enable line-length -->
