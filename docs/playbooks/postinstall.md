# Postinstall playbook

## Purpose

Generate needed files and host configuration after an ONAP deployment.
In particular, it will generate the "right" `/etc/hosts`.

## Inventory

The inventory must provide two groups `helm` and `kube-node`.

The first group is a group of servers where we'll configure `/etc/hosts`
according to ONAP deployment.

The second group will be used to retrieve needed IP addresses.

Search for vars files (`idf.yml` per default) happens at the same level as
inventory directory (see optional parameters to see how to change) per default:

```shell
.
├── inventory
│   └── inventory
└── vars
    └── idf.yml
    └── openstack_infos.yml
```

`openstack_infos.yml` is created by other roles (prepare and configure) from
this collection and is not mandatory. Although, it's "mandatory" when you want
to configure OpenStack related stuff.

If you don't have, please give right variables to role accordingly (see [role
documentation](../roles/generate_artifacts.md)).

## Mandatory parameters

None

## Optional parameters

On top of all variables from roles, the playbook adds the following variables:
<!-- markdownlint-disable line-length -->
| Variable            | Purpose                                  | Default value            |
|---------------------|------------------------------------------|--------------------------|
| `property_file`     | property file to load                    | `idf.yml`. Optional      |
| `base_dir`          | property file to load                    | `{{ inventory_dir }}/..` |
| `gather_nodes_fact` | to we gather facts from kubernetes nodes | true                     |
<!-- markdownlint-enable line-length -->
## Tags

The playbook proposes the following tags:

| Tag               | Purpose                            |
|-------------------|------------------------------------|
| `oom_postinstall` | for controlling postinstall launch |

## Examples

Inventory:

```ini
[helm]
my_server ansible_host: 1.2.3.4 ansible_user: toto

[kube-node]
my_other_server ansible_host: 1.2.3.5 ansible_user: toto
```

Launch:

```shell
ansible-playbook -i inventory/inventory playbooks/postinstall.yml

ansible-playbook -i inventory/inventory playbooks/postinstall.yml \
    --tags postinstall

ansible-playbook -i inventory/inventory playbooks/postinstall.yml \
    --extra-vars "base_dir=~"

ansible-playbook -i inventory/inventory playbooks/postinstall.yml \
    --extra-vars "{'openstack_user_name': 'test'}"
```
