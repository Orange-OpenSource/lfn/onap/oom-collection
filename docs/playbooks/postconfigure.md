# Postconfigure playbook

## Purpose

Create some resources in ONAP in order to have it usable just after

## Inventory

The inventory must provide a group `helm`. This is a group of servers that will
connect to Kubernetes cluster.

This playbook mandates ability to connect to the targeted Kubernetes via a kube
config file associated in default location
(`{{ ansible_user_dir }}/.kube/config`) for the chosen user on the servers in
`helm` group.

Search for vars files (`idf.yml` per default) happens at the same level as
inventory directory (see optional parameters to see how to change) per default:

```shell
.
├── inventory
│   └── inventory
└── vars
    └── idf.yml
    └── openstack_infos.yml
```

`openstack_infos.yml` is created by other roles (prepare and configure) from
this collection and is not mandatory. Although, it's "mandatory" when you want
to configure OpenStack related stuff.

If you don't have, please give right variables to role accordingly (see [role
documentation](../roles/postconfigure.md)).

## Mandatory parameters

None

## Optional parameters

On top of all variables from roles, the playbook adds the following variables:

| Variable        | Purpose               | Default value            |
|-----------------|-----------------------|--------------------------|
| `property_file` | property file to load | `idf.yml`. Optional      |
| `base_dir`      | property file to load | `{{ inventory_dir }}/..` |

## Tags

The playbook proposes the following tags:

| Tag                 | Purpose                              |
|---------------------|--------------------------------------|
| `oom_postconfigure` | for controlling postconfigure launch |

## Examples

Inventory:

```ini
[helm]
my_server ansible_host: 1.2.3.4 ansible_user: toto
```

Launch:

```shell
ansible-playbook -i inventory/inventory playbooks/postconfigure.yml

ansible-playbook -i inventory/inventory playbooks/postconfigure.yml \
    --tags postconfigure

ansible-playbook -i inventory/inventory playbooks/postconfigure.yml \
    --extra-vars "base_dir=~"

ansible-playbook -i inventory/inventory playbooks/postconfigure.yml \
    --extra-vars "{'onap_namespace': 'test'}"
```
