# Prepare playbook

## Purpose

Install external requirements and prepare the hosts for an ONAP deployment.

## Inventory

The inventory must provide a group `helm`. This is a group of servers that will
connect to Kubernetes cluster and remove ONAP resources.

This playbook mandates ability to connect to the targeted Kubernetes via a kube
config file associated in default location
(`{{ ansible_user_dir }}/.kube/config`) for the chosen user on the servers in
`helm` group.

Search for vars files (`idf.yml` per default) happens at the same level as
inventory directory (see optional parameters to see how to change) per default:

```shell
.
├── inventory
│   └── inventory
└── vars
    └── idf.yml
```

## Mandatory parameters

None

## Optional parameters

On top of all variables from roles, the playbook adds the following variables:

| Variable        | Purpose               | Default value            |
|-----------------|-----------------------|--------------------------|
| `property_file` | property file to load | `idf.yml`. Optional      |
| `base_dir`      | property file to load | `{{ inventory_dir }}/..` |

## Tags

The playbook proposes the following tags:

<!-- markdownlint-disable line-length -->
| Tag                         | Purpose                                           |
|-----------------------------|---------------------------------------------------|
| `oom_prepare`               | for controlling prepare launch                    |
| `oom_external_requirements` | for controlling external requirements launch only |
<!-- markdownlint-enable line-length -->

## Examples

Inventory:

```ini
[helm]
my_server ansible_host: 1.2.3.4 ansible_user: toto
```

Launch:

```shell
ansible-playbook -i inventory/inventory playbooks/clean.yml

ansible-playbook -i inventory/inventory playbooks/clean.yml --tags clean

ansible-playbook -i inventory/inventory playbooks/clean.yml \
    --extra-vars "base_dir=~"

ansible-playbook -i inventory/inventory playbooks/clean.yml \
    --extra-vars "{'onap_namespace': 'test'}"
```
