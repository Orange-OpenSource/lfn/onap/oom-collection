# Configure playbook

## Purpose

Configure an ONAP deployment before launch.

## Inventory

The inventory must provide two groups `helm` and `kube-node`.

The first group is a group of servers that will connect to the Kubernetes
cluster and generate overrides files used for ONAP installation.

The second group is not mandatory and is needed to set `minio.minio` IP address.

This playbook mandates ability to connect to the targeted Kubernetes via a kube
config file associated in default location
(`{{ ansible_user_dir }}/.kube/config`) for the chosen user on the server.

Search for vars files (`idf.yml` per default) happens at the same level as
inventory directory (see optional parameters to see how to change) per default:

```shell
.
├── inventory
│   └── inventory
└── vars
    └── idf.yml
    └── openstack_infos.yml
```

`openstack_infos.yml` is created by prepare role (prepare) from this collection
and is not mandatory. Although, it's "mandatory" when you want to configure
OpenStack related stuff.

If you don't have, please give right variables to role accordingly (see [role
documentation](../roles/configure.md)).

## Mandatory parameters

None

## Optional parameters

On top of all variables from roles, the playbook adds the following variables:
<!-- markdownlint-disable line-length -->
| Variable            | Purpose                                  | Default value            |
|---------------------|------------------------------------------|--------------------------|
| `property_file`     | property file to load                    | `idf.yml`. Optional      |
| `base_dir`          | property file to load                    | `{{ inventory_dir }}/..` |
| `gather_nodes_fact` | to we gather facts from kubernetes nodes | true                     |
<!-- markdownlint-enable line-length -->
## Tags

The playbook proposes the following tags:

| Tag             | Purpose                          |
|-----------------|----------------------------------|
| `oom_configure` | for controlling configure launch |

## Examples

Inventory:

```ini
[helm]
my_server ansible_host: 1.2.3.4 ansible_user: toto

[kube-node]
my_other_server ansible_host: 1.2.3.5 ansible_user: toto
```

Launch:

```shell
ansible-playbook -i inventory/inventory playbooks/configure.yml

ansible-playbook -i inventory/inventory playbooks/configure.yml --tags configure

ansible-playbook -i inventory/inventory playbooks/configure.yml \
    --extra-vars "base_dir=~"

ansible-playbook -i inventory/inventory playbooks/configure.yml \
    --extra-vars "{'openstack_user_name': 'test'}"
```
